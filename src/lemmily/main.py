
from ecs import EntityManager, SystemManager
from game import entity_factory
from game.systems import RenderingSystem, AnimationSystem

__author__ = 'Emily aka Lemmily'

import pyglet as pyg


class World():
    window =pyg.window.Window(800,600)
    batch = pyg.graphics.Batch()
    em = EntityManager()
    sm = SystemManager(em)

    def __init__(self):
        self.window.set_caption("PyggieSpaceWars Part 1")

world = World()
#

if __name__ == '__main__':

    #add the animation system to the system manager first, so it will be updated first.
    world.sm.add_system(AnimationSystem())
    #Rendering System being added to system manager
    r_system = RenderingSystem(world, world.batch)
    world.sm.add_system(r_system)

    #create our player entity in the center of the screen
    entity_factory.create_player(world, world.window.width/2, world.window.height/2)

    pyg.clock.schedule_interval(world.sm.update, 1/60.0)

    pyg.app.run()