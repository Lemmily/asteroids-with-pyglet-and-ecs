from pyglet.sprite import Sprite
from components import Transform, Renderable
from resources import player_image

__author__ = 'Emily aka Lemmily'


def create_player(world, x, y):
    """

    :param world:
    :param x:
    :param y:
    :return:
    """
    e = world.em.create_entity()

    player_ship = Sprite(player_image, x, y, batch=world.batch)
    player_ship.scale = 2

    world.em.add_component(e, Renderable(player_ship))
    world.em.add_component(e, Transform(x, y))

    return e
