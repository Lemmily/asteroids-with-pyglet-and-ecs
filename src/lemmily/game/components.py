__author__ = 'Emily'

from ecs import Component
import pyglet as pyg


class Renderable(Component):
    def __init__(self, sprite=None, batch=None):
        self.sprite = sprite
        if sprite == None:
            self.sprite = pyg.sprite.Sprite(batch=batch)


class Transform(Component):
    def __init__(self, x=0, y=0, rot=0):
        self.x = x
        self.y = y
        self.rotation = rot