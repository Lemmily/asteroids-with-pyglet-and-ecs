__author__ = 'Emily'

from ecs import System
from components import Renderable, Transform


class RenderingSystem(System):
    def __init__(self, world, batch):
        System.__init__(self)
        self.world = world
        self.batch = batch

    def update(self, dt):
        self.world.window.clear()
        self.batch.draw()


class AnimationSystem(System):
    def __init__(self):
        System.__init__(self)

    def update(self, dt):

        for entity, renderable_component in self.entity_manager.pairs_for_type(Renderable):
            transform_component = self.entity_manager.component_for_entity(entity, Transform)

            renderable_component.sprite.x = transform_component.x
            renderable_component.sprite.y = transform_component.y

            #so it doesnt do potentially costly rotations unnecessarily
            if renderable_component.sprite.rotation != transform_component.rotation:
                renderable_component.sprite.rotation = transform_component.rotation
