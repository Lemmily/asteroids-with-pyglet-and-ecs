from pyglet.gl import *

__author__ = 'Emily'

import pyglet as pyg



def set_anchor_point(image, anchor=None):
    """
    :param image: image to set the anchor point for
    :param anchor: position of anchor point in image, if None, defaults to center of image.
    :return:
    """
    if anchor == None:
        image.anchor_x = image.width/2
        image.anchor_y = image.height/2
    else:
        image.anchor_x = anchor[0]
        image.anchor_y = anchor[1]

pyg.resource.path = ["../data"]
pyg.resource.reindex()

player_image = pyg.resource.image("images/ship_1.png")

set_anchor_point(player_image)

glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST)
glEnable(GL_TEXTURE_2D)